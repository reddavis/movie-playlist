//
//  SearchBar.swift
//  Red Davis
//
//  Created by Red Davis on 12/05/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import SwiftUI


internal struct SearchBar: View
{
    // Internal
    @Binding var text: String
    @Binding var isActive: Bool
    
    // Private
    
    // MARK: Body
    
    internal var body: some View {
        HStack {
            HStack {
                Image(systemName: "magnifyingglass")
                
                TextField("Search", text: $text, onEditingChanged: { isEditing in
                    self.isActive = true
                }, onCommit: {
                    self.resignFirstResponder()
                }).foregroundColor(.primary)

                Button(action: {
                    self.text.removeAll()
                }) {
                    Image(systemName: "xmark.circle.fill").opacity(self.text.isEmpty ? 0.0 : 1.0)
                }
            }
            .padding(EdgeInsets(top: 8.0, leading: 8.0, bottom: 8.0, trailing: 8.0))
            .foregroundColor(.secondary)
            .background(Color(.secondarySystemBackground))
            .cornerRadius(10.0)

            if self.isActive
            {
                Button("Cancel") {
                    self.resignFirstResponder()
                    self.text.removeAll()
                    self.isActive = false
                }
                .foregroundColor(Color(.systemBlue))
            }
        }
        .padding(EdgeInsets(top: 8.0, leading: 16.0, bottom: 8.0, trailing: 16.0))
    }
}



// MARK: PreviewProvider

struct SearchBar_Previews: PreviewProvider
{
    static var previews: some View {
        SearchBar(text: .constant(""), isActive: .constant(true))
    }
}
