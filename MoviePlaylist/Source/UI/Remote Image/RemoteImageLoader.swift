//
//  RemoteImageLoader.swift
//  Red Davis
//
//  Created by Red Davis on 24/01/2021.
//

import Combine
import SwiftUI


final class RemoteImageLoader: ObservableObject
{
    // Static
    private static let processingQueue = DispatchQueue(label: "com.reddavis.RemoteImageLoader.processingQueue")
    
    // Internal
    let url: URL
    @Published var state: State = .idle
    
    // Private
    private var cancellable: AnyCancellable?
    
    // MARK: Initialization
    
    init(url: URL)
    {
        self.url = url
    }
    
    // MARK: Image
    
    func load()
    {
        if case State.loading = self.state { return }
        self.state = .loading
        
        self.cancellable = URLSession
            .shared
            .dataTaskPublisher(for: self.url)
            .subscribe(on: RemoteImageLoader.processingQueue)
            .map { UIImage(data: $0.data) }
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion
                {
                case .failure(let error):
                    self.state = .failed(error)
                case .finished:()
                }
            }, receiveValue: { image in
                guard let image = image else { self.state = .idle; return }
                self.state = .finishedLoading(image)
            })
    }
}


// MARK: State

extension RemoteImageLoader
{
    enum State
    {
        case idle
        case loading
        case finishedLoading(UIImage)
        case failed(URLError)
    }
}
