//
//  RemoteImage.swift
//  Red Davis
//
//  Created by Red Davis on 24/01/2021.
//

import SwiftUI


struct RemoteImage<Placeholder: View, Loading: View>: View
{
    // Private
    @ObservedObject private var imageLoader: RemoteImageLoader
    private let placeholder: Placeholder?
    private let loading: Loading?
    
    // MARK: Body
    
    var body: some View {
        Group {
            switch self.imageLoader.state
            {
            case .loading:
                self.loading
            case .failed:
                self.placeholder
            case .finishedLoading(let image):
                Image(uiImage: image)
                    .resizable()
            case .idle:
                self.placeholder
            }
        }
    }
    
    // MARK: Initialization
    
    init(url: URL,
         @ViewBuilder placeholder: () -> Placeholder,
         @ViewBuilder loading: () -> Loading)
         
    {
        self._imageLoader = ObservedObject(wrappedValue: RemoteImageLoader(url: url))
        self.placeholder = placeholder()
        self.loading = loading()
        
        self.imageLoader.load()
    }
}

// MARK: Equatable

extension RemoteImage: Equatable
{
    static func ==(lhs: RemoteImage, rhs: RemoteImage) -> Bool
    {
        lhs.imageLoader.url == rhs.imageLoader.url
    }
}



// MAKR: Preview

struct RemoteImage_Previews: PreviewProvider
{
    static var previews: some View {
        RemoteImage(url: URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png")!) {
            Image(systemName: "film")
        } loading: {
            ProgressView()
        }
    }
}
