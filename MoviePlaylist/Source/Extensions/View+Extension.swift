//
//  View+Extension.swift
//  MoviePlaylist
//
//  Created by Red Davis on 31/01/2021.
//

import SwiftUI


// MARK: Equatable

extension View where Self: Equatable
{
    func equatable() -> EquatableView<Self> { EquatableView(content: self) }
}


// MARK: Keyboard

extension View
{
    func resignFirstResponder()
    {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
