//
//  DataStore+MoviePlaylist.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Foundation


extension DataStore
{
    static func preview() -> DataStore
    {
        let store = DataStore()
        store.add(movies: [
            .fixture(),
            .fixture()
        ])
        
        return store
    }
}
