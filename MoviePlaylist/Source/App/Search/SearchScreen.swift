//
//  SearchScreen.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Combine
import SwiftUI


enum SearchScreen
{
    typealias ScreenViewModel = ViewModel<State, Event, Environment>
    
    static func make(identifier: UUID, environment: Environment = Environment()) -> SearchScreen.ContentView
    {
        ContentView(identifier: identifier, viewModel: ScreenViewModel(initialState: State(),
                                                                       reducer: self.reducer,
                                                                       environment: environment))
    }
}



// MARK: Reducer

extension SearchScreen
{
    static let reducer: Reducer<State, Event, Environment> = Reducer { state, event, environment in
        switch event
        {
        case .initialize:
            state.selectedMovies = environment.store.movies
        case .didFailToLoadSearchResults(let error):
            state.isLoading = false
            state.error = error
        case .moviesChanged(let movies):
            state.isLoading = false
            state.movies = movies
        case .queryChanged(let query):
            state.isLoading = true
            
            let request = SearchRequest(query: query, apiKey: environment.apiKey)
            return environment.apiClient.publisher(for: request)
                .subscribe(on: DispatchQueue.global())
                .receive(on: DispatchQueue.main)
                .map(\.results)
                .map(Event.moviesChanged)
                .catch { Just(Event.didFailToLoadSearchResults(ApplicationError.api($0))) }
                .eraseToAnyPublisher()
        case .addMovie(let movie):
            environment.store.add(movie: movie)
            state.selectedMovies = environment.store.movies
        case .removeMovie(let movie):
            environment.store.remove(movie: movie)
            state.selectedMovies = environment.store.movies
        }
        
        return nil
    }
}



// MARK: Environment

extension SearchScreen
{
    struct Environment
    {
        // Internal
        let apiKey: String
        let apiClient: MovieDatabaseAPIClient
        let store: DataStore

        // MARK: Initialization

        init(apiKey: String = ApplicationConfiguration.main.apiKey,
             apiClient: MovieDatabaseAPIClient = ApplicationConfiguration.main.apiClient,
             store: DataStore = ApplicationConfiguration.main.store)
        {
            self.apiKey = apiKey
            self.apiClient = apiClient
            self.store = store
        }
    }
}



// MARK: State

extension SearchScreen
{
    struct State
    {
        // Internal
        var movies: [Movie] = []
        var selectedMovies: [Movie] = []
        var isLoading: Bool = false
        var error: ApplicationError? = nil
    }
}


// MARK: Event

extension SearchScreen
{
    enum Event
    {
        case initialize
        case queryChanged(String)
        case moviesChanged([Movie])
        case didFailToLoadSearchResults(ApplicationError)
        case addMovie(Movie)
        case removeMovie(Movie)
    }
}
