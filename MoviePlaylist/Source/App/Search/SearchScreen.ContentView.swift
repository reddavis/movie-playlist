//
//  SearchScreen.ContentView.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Combine
import SwiftUI


extension SearchScreen
{
    struct ContentView: View
    {
        // Private
        private let identifier: UUID
        @SwiftUI.Environment(\.presentationMode) private var presentationMode
        @ObservedObject private var viewModel: ScreenViewModel
        @SwiftUI.State private var searchBarActive = false
        @SwiftUI.State private var searchQuery = ""
        
        private let searchQueryPublisher = CurrentValueSubject<String, Never>("")
        private var searchQueryCancellable: AnyCancellable? = nil
        
        // MARK: Body
        
        var body: some View {
            NavigationView {
                VStack {
                    SearchBar(text: self.$searchQuery, isActive: self.$searchBarActive)
                    
                    Group {
                        if self.viewModel.movies.isEmpty || self.searchQuery.isEmpty
                        {
                            self.emptyView()
                        }
                        else
                        {
                            List(self.viewModel.movies, id: \.id) { movie in
                                MovieRow(movie: movie, inPlaylist: self.viewModel.selectedMovies.contains(movie)) {
                                    if self.viewModel.selectedMovies.contains(movie)
                                    {
                                        self.viewModel.send(.removeMovie(movie))
                                    }
                                    else
                                    {
                                        self.viewModel.send(.addMovie(movie))
                                    }
                                }
                            }
                        }
                    }
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                }
                .onChange(of: self.searchQuery) {
                    self.searchQueryPublisher.send($0)
                }
                .gesture(
                    DragGesture()
                        .onChanged { _ in self.resignFirstResponder() }
                )
                .navigationBarTitle(Text("Search"), displayMode: .inline)
                .navigationBarItems(trailing: Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "xmark")
                }))
            }
        }
        
        // MARK: Initialization
        
        init(identifier: UUID, viewModel: ScreenViewModel)
        {
            self.identifier = identifier
            self._viewModel = ObservedObject(wrappedValue: viewModel)
            self.viewModel.send(.initialize)
            
            self.searchQueryCancellable = self.searchQueryPublisher
                .subscribe(on: DispatchQueue.global())
                .removeDuplicates()
                .filter { !$0.isEmpty }
                .debounce(for: 0.4, scheduler: DispatchQueue.main)
                .sink { viewModel.send(.queryChanged($0)) }
        }
        
        // MARK: UI
        
        private func emptyView() -> some View
        {
            Group {
                if self.viewModel.isLoading
                {
                    ProgressView()
                }
                else if self.self.searchQueryPublisher.value.isEmpty
                {
                    Text("Search for a film 🕵️")
                        .font(.footnote)
                        .foregroundColor(.secondary)
                }
                else
                {
                    Text("No movies found 😭")
                        .font(.footnote)
                        .foregroundColor(.secondary)
                }
            }
        }
    }
}

extension SearchScreen.ContentView: Equatable
{
    static func == (lhs: SearchScreen.ContentView, rhs: SearchScreen.ContentView) -> Bool { lhs.identifier == rhs.identifier }
}



// MARK: Preview

struct SearchScreen_ContentView_Previews: PreviewProvider
{
    static var previews: some View {
        NavigationView {
            SearchScreen.make(identifier: UUID(), environment: SearchScreen.Environment(apiClient: ApplicationConfiguration.preview.apiClient,
                                                                                        store: ApplicationConfiguration.preview.store))
        }
    }
}
