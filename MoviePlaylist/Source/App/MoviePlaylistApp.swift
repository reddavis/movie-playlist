//
//  MoviePlaylistApp.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import SwiftUI


@main
struct MoviePlaylistApp: App
{
    var body: some Scene {
        WindowGroup {
            RootScreen.make()
        }
    }
}
