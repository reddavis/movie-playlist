//
//  MovieRow.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import SwiftUI


struct MovieRow: View
{
    // Internal
    let movie: Movie
    let inPlaylist: Bool
    let onAction: () -> Void
    
    // MARK: Body
    
    var body: some View {
        HStack(spacing: 16.0) {
            self.poster()
            
            VStack(alignment: .leading, spacing: 4.0) {
                Text(self.movie.title)
                Text(self.movie.year)
                    .font(.footnote)
            }
            
            Spacer()
            
            Button(action: self.onAction, label: {
                Text(self.inPlaylist ? "Remove" : "Add")
                    .font(.footnote)
                    .bold()
                    .foregroundColor(self.inPlaylist ? .red : .blue)
                    .frame(minWidth: 50.0)
                    .padding(EdgeInsets(top: 8.0, leading: 16.0, bottom: 8.0, trailing: 16.0))
                    .background(Color.white)
                    .clipShape(Capsule())
                    .shadow(color: .gray, radius: 3, x: 0.0, y: 1.0)
            })
        }
        .buttonStyle(PlainButtonStyle())
        .frame(height: 100.0)
        .padding([.top, .bottom], 8.0)
    }
    
    // MARK: UI
    
    private func poster() -> some View
    {
        Group {
            if let url = self.movie.posterURL
            {
                RemoteImage(url: url) {
                    Image(systemName: "film")
                } loading: {
                    ProgressView()
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
        }
        .frame(maxHeight: .infinity)
        .aspectRatio(CGSize(width: 2, height: 3), contentMode: .fit)
        .background(Color.gray)
        .cornerRadius(3.0)
        .shadow(color: .black, radius: 3, x: 0.0, y: 1.0)
    }
}



// MARK: Preview

struct MovieRow_Previews: PreviewProvider
{
    static var previews: some View {
        List {
            MovieRow(movie: .fixture(), inPlaylist: true) { }
            MovieRow(movie: .fixture(), inPlaylist: false) { }
        }
        
    }
}
