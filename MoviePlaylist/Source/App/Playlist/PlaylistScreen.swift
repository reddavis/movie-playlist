//
//  PlaylistScreen.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Combine
import SwiftUI


enum PlaylistScreen
{
    typealias ScreenViewModel = ViewModel<State, Event, Environment>
    
    static func make(environment: Environment = Environment()) -> some View
    {
        ContentView(viewModel: ScreenViewModel(initialState: State(),
                                               reducer: self.reducer,
                                               environment: environment))
    }
}



// MARK: Reducer

extension PlaylistScreen
{
    static let reducer: Reducer<State, Event, Environment> = Reducer { state, event, environment in
        switch event
        {
        case .initialize:
            return environment
                .store
                .moviesPublisher
                .subscribe(on: DispatchQueue.global())
                .receive(on: DispatchQueue.main)
                .map(Event.moviesChanged)
                .eraseToAnyPublisher()
        case .moviesChanged(let movies):
            state.movies = movies
        case .removeMovie(let movie):
            environment.store.remove(movie: movie)
        }
        
        return nil
    }
}



// MARK: Environment

extension PlaylistScreen
{
    struct Environment
    {
        // Internal
        let store: DataStore

        // MARK: Initialization

        init(store: DataStore = ApplicationConfiguration.main.store)
        {
            self.store = store
        }
    }
}



// MARK: State

extension PlaylistScreen
{
    struct State: Equatable
    {
        // Internal
        var movies: [Movie] = []
    }
}


// MARK: Event

extension PlaylistScreen
{
    enum Event
    {
        case initialize
        case moviesChanged([Movie])
        case removeMovie(Movie)
    }
}
