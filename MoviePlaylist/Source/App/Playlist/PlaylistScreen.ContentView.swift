//
//  PlaylistScreen.ContentView.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import SwiftUI


extension PlaylistScreen
{
    struct ContentView: View
    {
        // Private
        private let searchScreenIdentifier = UUID()
        @ObservedObject private var viewModel: ScreenViewModel
        @SwiftUI.State private var searchPresented = false
        @SwiftUI.State private var searchScreen: SearchScreen.ContentView?
        
        // MARK: Body
        
        var body: some View {
            NavigationView {
                ZStack(alignment: .bottomTrailing) {
                    Group {
                        if self.viewModel.movies.isEmpty
                        {
                            Text("No movies added yet!")
                                .font(.footnote)
                                .foregroundColor(.secondary)
                        }
                        else
                        {
                            List(self.viewModel.movies, id: \.id) { movie in
                                MovieRow(movie: movie, inPlaylist: true) {
                                    self.viewModel.send(.removeMovie(movie))
                                }
                            }
                        }
                    }
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    
                    self.searchButton()
                        .offset(x: -40.0, y: -10.0)
                }
                .navigationBarTitleDisplayMode(.large)
                .navigationTitle(Text("Your playlist"))
                .sheet(isPresented: self.$searchPresented, content: {
                    SearchScreen.make(identifier: self.searchScreenIdentifier)
                })
                .onAppear { self.viewModel.send(.initialize) }
            }
        }
        
        // MARK: Initialization
        
        init(viewModel: ScreenViewModel)
        {
            self._viewModel = ObservedObject(wrappedValue: viewModel)
        }
        
        // MARK: UI
        
        private func searchButton() -> some View
        {
            Button(action: self.presentSearchScreen, label: {
                Image(systemName: "magnifyingglass")
                    .resizable()
                    .frame(width: 16.0, height: 16.0)
                    .foregroundColor(.primary)
                    .padding()
                    .background(Color.white)
                    .clipShape(Circle())
                    .shadow(color: Color.black, radius: 3, x: 0.0, y: 1.0)
            })
        }
        
        // MARK: Presentation
        
        private func presentSearchScreen()
        {
            self.searchPresented = true
        }
    }
}



// MARK: Preview

struct PlaylistScreen_ContentView_Previews: PreviewProvider
{
    static var previews: some View {
        NavigationView {
            PlaylistScreen.make(environment: PlaylistScreen.Environment(store: ApplicationConfiguration.preview.store))
        }
    }
}
