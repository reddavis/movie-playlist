//
//  RootScreen.ContentView.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import SwiftUI


extension RootScreen
{
    struct ContentView: View
    {
        // MARK: Body
        
        var body: some View {
            PlaylistScreen.make()
        }
    }
}



// MARK: Preview

struct RootScreen_ContentView_Previews: PreviewProvider
{
    static var previews: some View {
        return RootScreen.make()
    }
}
