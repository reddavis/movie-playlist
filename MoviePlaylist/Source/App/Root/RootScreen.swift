//
//  RootScreen.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import SwiftUI


enum RootScreen
{
    static func make() -> some View
    {
        ContentView()
    }
}
