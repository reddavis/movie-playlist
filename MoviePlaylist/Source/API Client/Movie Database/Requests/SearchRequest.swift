//
//  SearchRequest.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Foundation


struct SearchRequest: HTTPRequest
{
    // Internal
    typealias ResponseObject = SearchResponse
    typealias ErrorObject = MovieDatabaseAPIError
    
    var method: HTTPMethod { .get }
    var handlerDispatchQueue: DispatchQueue = .main
    var path: String { "/" }
    var isAuthenticated: Bool { false }
    var body: Data?
    var queryItems: [URLQueryItem]?
    
    // MARK: Initialization
    
    init(query: String, apiKey: String)
    {
        self.queryItems = [
            URLQueryItem(name: "apiKey", value: apiKey),
            URLQueryItem(name: "s", value: query),
            URLQueryItem(name: "type", value: "movie"),
        ]
    }
}
