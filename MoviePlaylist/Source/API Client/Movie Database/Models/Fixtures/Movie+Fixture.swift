//
//  Movie+Fixture.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Foundation


extension Movie: Fixture
{
    static func fixture(_ configure: ((inout Movie) -> Void)? = nil) -> Movie
    {
        var fixture = Movie(id: UUID().uuidString,
                            title: "The Matrix",
                            year: "1999",
                            posterURL: URL(string: "https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg"))
        configure?(&fixture)
        return fixture
    }
}
