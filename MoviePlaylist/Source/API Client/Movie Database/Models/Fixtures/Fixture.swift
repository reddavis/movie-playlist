//
//  Fixture.swift
//  Red Davis
//
//  Created by Red Davis on 13/01/2021.
//

import Foundation


protocol Fixture
{
    static func fixture(_ configure: ((_ fixture: inout Self) -> Void)?) -> Self
}
