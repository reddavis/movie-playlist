//
//  SearchResponse.swift
//  MoviePlaylist
//
//  Created by Red Davis on 31/01/2021.
//

import Foundation


struct SearchResponse: Codable
{
    // Internal
    var results: [Movie]
    var numberOfResults: Int
    
    // MARK: Initialization
    
    init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.results = try container.decode(.results)
        
        let numberOfResultsRawValue: String = try container.decode(.numberOfResults)
        if let numberOfResults = Int(numberOfResultsRawValue)
        {
            self.numberOfResults = numberOfResults
        }
        else
        {
            throw DecodingError.dataCorruptedError(forKey: CodingKeys.numberOfResults, in: container, debugDescription: "numberOfResults invalid")
        }
    }
}

// MARK: Coding keys

extension SearchResponse
{
    enum CodingKeys: String, CodingKey
    {
        case results = "Search"
        case numberOfResults = "totalResults"
    }
}
