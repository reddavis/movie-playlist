//
//  MovieDatabaseAPIError.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Foundation


struct MovieDatabaseAPIError: LocalizedError, Codable
{
    // Static
    static let unknown = MovieDatabaseAPIError(message: "Unknown error")
    static let noData = MovieDatabaseAPIError(message: "No data")
    static let invalidJSON = MovieDatabaseAPIError(message: "Invalid JSON")
    static let noInternetConnection = MovieDatabaseAPIError(message: "No internet connection")
    
    // Internal
    let message: String
}

// MARK: Equatable

extension MovieDatabaseAPIError: Equatable { }

// MARK: Coding keys

extension MovieDatabaseAPIError
{
    enum CodingKeys: String, CodingKey
    {
        case message = "Error"
    }
}
