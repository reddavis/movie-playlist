//
//  Movie.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Foundation


struct Movie: Codable
{
    // Internal
    var id: String
    var title: String
    var year: String
    var posterURL: URL?
}

// MARK: Equatable

extension Movie: Equatable
{
    static func ==(lhs: Movie, rhs: Movie) -> Bool { lhs.id == rhs.id }
}

// MARK: Hashable

extension Movie: Hashable
{
    func hash(into hasher: inout Hasher) { hasher.combine(self.id) }
}

// MARK: Coding keys

extension Movie
{
    enum CodingKeys: String, CodingKey
    {
        case id = "imdbID"
        case year = "Year"
        case title = "Title"
        case posterURL = "Poster"
    }
}
