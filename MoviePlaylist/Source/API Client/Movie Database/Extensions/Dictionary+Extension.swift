//
//  Dictionary.swift
//  Red Davis
//
//  Created by Red Davis on 17/10/2020.
//

import Foundation


extension Dictionary
{
    /// Performs a merging of two dictionarys.
    /// If both dictionarys have the same key this function
    /// will always use the `other` dictionary's value.
    /// - Parameter dictionary: A `Dictionary` instance.
    /// - Returns: A `Dictionary` instance.
    func merging(other dictionary: [Key : Value]) -> [Key : Value]
    {
        return self.merging(dictionary) { $1 }
    }
    
    /// Performs a mutating merging of two dictionarys.
    /// If both dictionarys have the same key this function
    /// will always use the `other` dictionary's value.
    /// - Parameter dictionary: A `Dictionary` instance.
    mutating func merge(other dictionary: [Key : Value])
    {
        self.merge(dictionary) { $1 }
    }
}
