//
//  DecodingContainer+Extension.swift
//  Red Davis
//
//  Created by Red Davis on 24/09/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Foundation


extension KeyedDecodingContainer
{
    /// Decodes a value of the inferred type for the given key.
    /// - Parameter key: The key that the decoded value is associated with.
    /// - Throws:
    ///     - Error on decoding.
    /// - Returns: The decoded value.
    func decode<T: Decodable>(_ key: KeyedDecodingContainer<K>.Key) throws -> T
    {
        return try self.decode(T.self, forKey: key)
    }
    
    /// Decodes a value of the inferred type for the given key, if present.
    /// - Parameter key: The key that the decoded value is associated with.
    /// - Throws:
    ///     - Error on decoding.
    /// - Returns: The decoded value.
    func decodeIfPresent<T: Decodable>(_ key: KeyedDecodingContainer<K>.Key) throws -> T?
    {
        return try self.decodeIfPresent(T.self, forKey: key)
    }
}



// MARK: SingleValueDecodingContainer

extension SingleValueDecodingContainer
{
    /// Decodes a single value of the inferred type.
    /// - Throws:
    ///     - Error on decoding.
    /// - Returns: The decoded value.
    func decode<T: Decodable>() throws -> T
    {
        return try self.decode(T.self)
    }
}
