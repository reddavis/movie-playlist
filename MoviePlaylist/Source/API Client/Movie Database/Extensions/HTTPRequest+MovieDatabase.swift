//
//  HTTPRequest+MovieDatabase.swift
//  DiagnosticReporter
//
//  Created by Red Davis on 17/10/2020.
//

import Foundation


extension HTTPRequest
{
    // Public
    var baseURL: URL { URL(string: "https://www.omdbapi.com/")! }
    var method: HTTPMethod { .get }
    var queryItems: [URLQueryItem]? { nil }
    var headers: [String : String]? { nil }
    var timeout: TimeInterval { 30.0 }
}

// MARK: Error construction

extension HTTPRequest
{
    func buildError(data: Data?) -> MovieDatabaseAPIError
    {
        guard let data = data else { return .noData }
        
        // Parse data
        do
        {
            let decoder = JSONDecoder.movieDatabase()
            return try decoder.decode(MovieDatabaseAPIError.self, from: data)
        }
        catch
        {
            return .invalidJSON
        }
    }
}

// MARK: Default processing where ErrorObject == MovieDatabaseAPIError, ResponseObject: Decodable

extension HTTPRequest where ErrorObject == MovieDatabaseAPIError, ResponseObject: Codable
{
    func process(data: Data, response: HTTPURLResponse) -> Result<ResponseObject, ErrorObject>
    {
        guard response.isSuccess else { return .failure(self.buildError(data: data)) }
        
        do
        {
            let decoder = JSONDecoder.movieDatabase()
            let response = try decoder.decode(ResponseObject.self, from: data)
            return .success(response)
        }
        catch
        {
            return .failure(self.buildError(data: data))
        }
    }
}
