//
//  MovieDatabaseAPIClient.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Combine
import Foundation


final class MovieDatabaseAPIClient
{
    // Private
    private let session: HTTPSession
    private let networkMonitor: NetworkMonitorProtocol
    private let requestQueue = DispatchQueue(label: "MovieDatabaseAPIClient.requestQueue.\(UUID().uuidString)")
    
    private let defaultHeaders = [
        "Content-Type" : "application/json"
    ]
    
    // MARK: Initialization
    
    required init(session: HTTPSession, networkMonitor: NetworkMonitorProtocol = NetworkMonitor())
    {
        self.session = session
        self.networkMonitor = networkMonitor
    }
    
    // MARK: Request
    
    func publisher<T: HTTPRequest>(for request: T) -> AnyPublisher<T.ResponseObject, T.ErrorObject> where T.ErrorObject == MovieDatabaseAPIError, T.ResponseObject: Codable
    {
        // Check if there is network connectivity
        guard self.networkMonitor.isConnected else
        {
            return Fail(error: MovieDatabaseAPIError.noInternetConnection)
                .eraseToAnyPublisher()
        }
        
        return self.requestPublisher(for: request)
    }
     
    private func requestPublisher<T: HTTPRequest>(for request: T) -> AnyPublisher<T.ResponseObject, T.ErrorObject> where T.ErrorObject == MovieDatabaseAPIError, T.ResponseObject: Codable
    {
        self.sessionDataPublisher(for: request)
            .subscribe(on: self.requestQueue)
            .share()
            .tryMap { data, response -> (data: Data, response: HTTPURLResponse) in
                guard let httpResponse = response as? HTTPURLResponse else { throw MovieDatabaseAPIError.unknown }
                return (data, response: httpResponse)
            }
            .map { request.process(data: $0.data, response: $0.response) }
            .tryMap { result in
                switch result
                {
                case .failure(let error):
                    throw error
                case .success(let object):
                    return object
                }
            }
            .mapError { error in
                guard let error = error as? MovieDatabaseAPIError else { return MovieDatabaseAPIError.unknown }
                return error
            }
            .receive(on: request.handlerDispatchQueue)
            .eraseToAnyPublisher()
    }
    
    private func sessionDataPublisher<T: HTTPRequest>(for request: T) -> AnyPublisher<HTTPSession.DataTaskPublisherOutput, URLError>
    {
        var urlComponents = URLComponents(url: request.baseURL, resolvingAgainstBaseURL: false)!
        urlComponents.path = request.path
        urlComponents.queryItems = request.queryItems
        
        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.allHTTPHeaderFields = self.buildHeaders(for: request)
        urlRequest.httpBody = request.body
        urlRequest.timeoutInterval = request.timeout
        
        return self.session.dataTaskPublisher(for: urlRequest)
    }
    
    private func buildHeaders<T>(for request: T) -> [String : String] where T: HTTPRequest
    {
        guard let additionalHeaders = request.headers else { return self.defaultHeaders  }
        return self.defaultHeaders.merging(other: additionalHeaders)
    }
}
