//
//  URLSession+HTTPSession.swift
//  Red Davis
//
//  Created by Red Davis on 24/09/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Combine
import Foundation


extension URLSession: HTTPSession
{
    func sessionDataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> HTTPSessionDataTask
    {
        self.dataTask(with: request, completionHandler: completionHandler)
    }
    
    func dataTaskPublisher(for request: URLRequest) -> AnyPublisher<HTTPSession.DataTaskPublisherOutput, URLError>
    {
        let publisher: URLSession.DataTaskPublisher = self.dataTaskPublisher(for: request)
        return publisher.eraseToAnyPublisher()
    }
}
