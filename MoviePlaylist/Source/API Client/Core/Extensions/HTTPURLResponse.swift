//
//  HTTPURLResponse.swift
//  Red Davis
//
//  Created by Red Davis on 17/10/2020.
//

import Foundation


extension HTTPURLResponse
{
    // Internal
    var isSuccess: Bool { 200...299 ~= self.statusCode }
    
    // MARK: Header
    
    /// Returns a value for the provided header. The header key
    /// is treated as case-insensitive. This is a workaround
    /// for bug: https://bugs.swift.org/browse/SR-2429
    /// - Parameter header: A `String` instance.
    /// - Returns: The requested header value.
    func value(forHeader header: String) -> Any?
    {
        let headers = self.allHeaderFields.reduce(into: [String : Any]()) { (result, keyValue) in
            guard let key = keyValue.key as? String else { return }
            result[key.lowercased()] = keyValue.value
        }
        
        return headers[header.lowercased()]
    }
}
