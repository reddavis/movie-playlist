//
//  NetworkMonitor.swift
//  Red Davis
//
//  Created by Red Davis on 15/05/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Foundation
import Network


/// Monitors network connectivity.
final class NetworkMonitor: NetworkMonitorProtocol
{
    // Public
    weak var delegate: NetworkMonitorDelegate?
    
    /// Indicates whether there is network connectivity or not.
    var isConnected: Bool {
        return self.monitor.currentPath.status == .satisfied
    }
    
    // Private
    private let queue = DispatchQueue(label: UUID().uuidString)
    private let monitor = NWPathMonitor()
    
    // MARK: Initialization
    
    /// Initializes a new `NetworkMonitor` instance.
    required init()
    {
        self.monitor.pathUpdateHandler = { [weak self] path in
            guard let self = self else { return }
            
            let isConnected = path.status == .satisfied
            self.delegate?.networkStatusChanged(isConnected: isConnected, monitor: self)
        }
        
        self.monitor.start(queue: self.queue)
    }
    
    deinit
    {
        self.monitor.cancel()
    }
}
