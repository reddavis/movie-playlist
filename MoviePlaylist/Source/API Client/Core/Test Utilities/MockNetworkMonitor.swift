//
//  MockNetworkMonitor.swift
//  Red Davis
//
//  Created by Red Davis on 11/11/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Foundation


/// A mock network monitor. Can be used to simulate network connection.
struct MockNetworkMonitor: NetworkMonitorProtocol
{
    var isConnected: Bool
    var delegate: NetworkMonitorDelegate?
    
    // MARK: Initialization
    
    init(isConnected: Bool)
    {
        self.isConnected = isConnected
    }
}
