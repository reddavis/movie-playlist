//
//  HTTPRequest.swift
//  Red Davis
//
//  Created by Red Davis on 15/05/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Foundation


/// A protocol for defining a HTTP request.
protocol HTTPRequest
{
    associatedtype ResponseObject: Any
    associatedtype ErrorObject: Error
 
    typealias CompletionHandler = (_ result: Result<ResponseObject, ErrorObject>) -> Void
    
    /// The base URL.
    var baseURL: URL { get }
    
    /// The URL path.
    var path: String { get }
    
    /// The HTTP method used in the request.
    var method: HTTPMethod { get }
    
    /// An array of query items to be appended to the URL path.
    var queryItems: [URLQueryItem]? { get }
    
    /// `Dictionary` of headers.
    var headers: [String : String]? { get }
    
    /// A `TimeInterval` of when the request should timeout.
    var timeout: TimeInterval { get }
    
    /// A `Bool` to indicate whether the request is authenticated or not.
    var isAuthenticated: Bool { get }
    
    /// HTTP body of the request.
    var body: Data? { get set }
    
    /// The `DispatchQueue` that the `handler` will get called on.
    var handlerDispatchQueue: DispatchQueue { get set }
    
    /// Processes the response.
    /// - Parameters:
    ///   - data: The `Data` returned by the server.
    ///   - response: The `HTTPURLResponse` returned.
    func process(data: Data, response: HTTPURLResponse) -> Result<ResponseObject, ErrorObject>
}
