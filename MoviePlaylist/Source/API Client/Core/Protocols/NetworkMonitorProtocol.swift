//
//  NetworkMonitorProtocol.swift
//  Red Davis
//
//  Created by Red Davis on 11/11/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Foundation


protocol NetworkMonitorProtocol
{
    /// The delegate of the network monitor.
    var delegate: NetworkMonitorDelegate? { get set }
    
    /// Indicates whether there is network connectivity or not.
    var isConnected: Bool { get }
}



// MARK: NetworkMonitorDelegate

protocol NetworkMonitorDelegate: class
{
    func networkStatusChanged(isConnected: Bool, monitor: NetworkMonitorProtocol)
}
