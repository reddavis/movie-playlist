//
//  HTTPSessionDataTask.swift
//  Red Davis
//
//  Created by Red Davis on 15/05/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Foundation


/// A shell protocol of `URLSessionDataTask`. Primarily used to make testing easier.
protocol HTTPSessionDataTask
{
    func resume()
    func cancel()
}
