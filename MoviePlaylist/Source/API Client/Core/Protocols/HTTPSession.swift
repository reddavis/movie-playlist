//
//  HTTPSession.swift
//  Red Davis
//
//  Created by Red Davis on 15/05/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Combine
import Foundation


/// A shell protocol. Primarily used to make testing easier.
protocol HTTPSession
{
    var delegate: URLSessionDelegate? { get }
    func sessionDataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> HTTPSessionDataTask
    
    typealias DataTaskPublisherOutput = (data: Data, response: URLResponse)
    func dataTaskPublisher(for request: URLRequest) -> AnyPublisher<DataTaskPublisherOutput, URLError>
}
