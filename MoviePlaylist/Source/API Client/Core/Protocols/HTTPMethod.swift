//
//  HTTPMethod.swift
//  Red Davis
//
//  Created by Red Davis on 15/05/2020.
//  Copyright © 2020 Red Davis. All rights reserved.
//

import Foundation


enum HTTPMethod: String
{
    /// `GET` method.
    case get = "GET"
    
    /// `POST` method.
    case post = "POST"
    
    /// `DELETE` method.
    case delete = "DELETE"
    
    /// `PUT` method.
    case put = "PUT"
    
    /// `HEAD` method.
    case head = "HEAD"
    
    /// `TRACE` method.
    case track = "TRACE"
    
    /// `PATCH` method.
    case patch = "PATCH"
    
    /// `OPTIONS` method.
    case options = "OPTIONS"
    
    /// `CONNECT` method.
    case connect = "CONNECT"
}
