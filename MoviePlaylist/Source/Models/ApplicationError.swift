//
//  ApplicationError.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Foundation


enum ApplicationError: LocalizedError
{
    case api(MovieDatabaseAPIError)
    case standard(Error)
    
    var errorDescription: String? {
        switch self
        {
        case .api(let error):
            return error.localizedDescription
        case .standard(let error):
            return error.localizedDescription
        }
    }
}
