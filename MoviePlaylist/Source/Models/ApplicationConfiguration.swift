//
//  ApplicationConfiguration.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Foundation


struct ApplicationConfiguration
{
    // Static
    static let main = ApplicationConfiguration(apiKey: "e8c23204",
                                               apiClient: MovieDatabaseAPIClient(session: URLSession(configuration: .default)),
                                               store: DataStore())
    static let preview = ApplicationConfiguration(apiKey: "",
                                                  apiClient: MovieDatabaseAPIClient(session: MockHTTPSession.preview(),
                                                                                    networkMonitor: MockNetworkMonitor(isConnected: true)),
                                                  store: .preview())
    
    // Internal
    let apiKey: String
    let apiClient: MovieDatabaseAPIClient
    let store: DataStore
}
