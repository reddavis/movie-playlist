//
//  ViewModel.swift
//  Red Davis
//
//  Created by Red Davis on 30/12/2020.
//

import Foundation
import Combine


@dynamicMemberLookup
final class ViewModel<State, Event, Environment>: ObservableObject
{
    // Internal
    @Published var state: State
    let environment: Environment
    
    // Private
    private let reducer: Reducer<State, Event, Environment>
    private var cancellables: Set<AnyCancellable> = []
    
    // MARK: Initialization
    
    init(initialState: State,
         reducer: Reducer<State, Event, Environment>,
         environment: Environment)
    {
        self.state = initialState
        self.reducer = reducer
        self.environment = environment
    }

    func send(_ event: Event)
    {
        guard let effect = self.reducer.execute(state: &self.state, event: event, environment: self.environment) else { return }
        effect
            .sink(receiveValue: self.send)
            .store(in: &self.cancellables)
    }

    subscript<U>(dynamicMember keyPath: KeyPath<State, U>) -> U
    {
        return self.state[keyPath: keyPath]
    }
}


// MARK: Reducer

struct Reducer<State, Event, Environment>
{
    // Private
    private let reduce: (inout State, Event, Environment) -> AnyPublisher<Event, Never>?

    // MARK: Initialization
    
    init(_ reduce: @escaping (inout State, Event, Environment) -> AnyPublisher<Event, Never>?)
    {
        self.reduce = reduce
    }

    // MARK: API
    
    func execute(state: inout State,
                 event: Event,
                 environment: Environment) -> AnyPublisher<Event, Never>?
    {
        self.reduce(&state, event, environment)
    }
}
