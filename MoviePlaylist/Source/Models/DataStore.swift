//
//  DataStore.swift
//  MoviePlaylist
//
//  Created by Red Davis on 30/01/2021.
//

import Combine
import Foundation


final class DataStore
{
    // Internal
    let moviesPublisher = CurrentValueSubject<[Movie], Never>([])
    
    private(set) var movies: [Movie] = [] {
        didSet { self.moviesPublisher.send(self.movies) }
    }
    
    // MARK: API
    
    func add(movie: Movie)
    {
        self.movies.append(movie)
    }
    
    func add(movies: [Movie])
    {
        self.movies.append(contentsOf: movies)
    }
    
    func remove(movie: Movie)
    {
        guard let index = self.movies.firstIndex(of: movie) else { return }
        self.movies.remove(at: index)
    }
    
    func remove(movies: [Movie])
    {
        self.movies.removeAll { movies.contains($0) }
    }
}
