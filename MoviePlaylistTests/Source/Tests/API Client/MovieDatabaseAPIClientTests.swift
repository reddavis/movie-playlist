//
//  MovieDatabaseAPIClientTests.swift
//  MoviePlaylistTests
//
//  Created by Red Davis on 31/01/2021.
//

import Combine
import XCTest
@testable import MoviePlaylist


final class MovieDatabaseAPIClientTests: XCTestCase
{
    // Private
    private var session: MockHTTPSession!
    private var apiClient: MovieDatabaseAPIClient!
    private var cancellables: Set<AnyCancellable>!
    
    // MARK: Setup
    
    override func setUpWithError() throws
    {
        self.cancellables = []
        self.session = MockHTTPSession()
        self.apiClient = MovieDatabaseAPIClient(session: self.session,
                                                networkMonitor: MockNetworkMonitor(isConnected: true))
    }
    
    // MARK: Tests
    
    func testExecutingRequests() throws
    {
        let expectation = self.expectation(description: "Completion")
        
        self.session.register(stub: MockHTTPSession.Stub(path: "/test",
                                                         method: .get,
                                                         statusCode: 200,
                                                         data: try MockResponse().encode()))
        
        let request = MockRequest()
        self.apiClient
            .publisher(for: request)
            .sink {
                switch $0
                {
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                case .finished:()
                }
            } receiveValue: { _ in
                expectation.fulfill()
            }
            .store(in: &self.cancellables)

        self.waitForExpectations(timeout: 5.0)
    }
    
    func testHandlingFailedRequests() throws
    {
        let expectation = self.expectation(description: "Completion")
        self.session.register(stub: MockHTTPSession.Stub(path: "/test",
                                                         method: .get,
                                                         statusCode: 400,
                                                         data: try MovieDatabaseAPIError.noInternetConnection.encode()))
        
        let request = MockRequest()
        self.apiClient
            .publisher(for: request)
            .sink {
                XCTAssertEqual($0, .failure(.noInternetConnection))
                expectation.fulfill()
            } receiveValue: { _ in
                XCTFail()
            }
            .store(in: &self.cancellables)

        self.waitForExpectations(timeout: 5.0)
    }
}
