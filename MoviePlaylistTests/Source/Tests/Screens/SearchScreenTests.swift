//
//  SearchScreenTests.swift
//  MoviePlaylistTests
//
//  Created by Red Davis on 31/01/2021.
//

import Combine
import XCTest
@testable import MoviePlaylist


class SearchScreenTests: XCTestCase
{
    // Private
    private var store: DataStore!
    private var session: MockHTTPSession!
    private var apiClient: MovieDatabaseAPIClient!
    private var viewModel: ViewModel<SearchScreen.State, SearchScreen.Event, SearchScreen.Environment>!
    
    // Setup
    
    override func setUpWithError() throws
    {
        self.session = MockHTTPSession()
        self.apiClient = MovieDatabaseAPIClient(session: self.session,
                                                networkMonitor: MockNetworkMonitor(isConnected: true))
        self.store = DataStore()
        self.viewModel = ViewModel(initialState: SearchScreen.State(),
                                   reducer: SearchScreen.reducer,
                                   environment: SearchScreen.Environment(apiKey: "abc",
                                                                         apiClient: self.apiClient,
                                                                         store: self.store))
    }

    // MARK: Tests
    
    func testInitializationEvent() throws
    {
        let movie = Movie.fixture()
        
        XCTAssertEqual(self.viewModel.selectedMovies.count, 0)
        self.store.add(movie: movie)
        self.viewModel.send(.initialize)
        XCTAssertEqual(self.viewModel.selectedMovies, [movie])
    }
    
    func testDidFailToLoadSearchResultsEvent() throws
    {
        XCTAssertNil(self.viewModel.error)
        self.viewModel.send(.didFailToLoadSearchResults(.api(.noInternetConnection)))
        
        guard let error = self.viewModel.error,
              case ApplicationError.api(let apiError) = error,
              apiError == .noInternetConnection  else
        {
            XCTFail("Error incorrect: Expected to be MovieDatabaseAPIError.noInternetConnection")
            return
        }
    }
    
    func testMoviesChangedEvent() throws
    {
        XCTAssertEqual(self.viewModel.movies.count, 0)
        self.viewModel.send(.moviesChanged([.fixture()]))
        XCTAssertEqual(self.viewModel.movies.count, 1)
    }

    func testRemoveMovieEvent() throws
    {
        let movie = Movie.fixture()
        self.store.add(movie: movie)
        
        XCTAssertEqual(self.viewModel.selectedMovies.count, 0)
        self.viewModel.send(.initialize)
        XCTAssertEqual(self.viewModel.selectedMovies.count, 1)
        
        self.viewModel.send(.removeMovie(movie))
        XCTAssertEqual(self.viewModel.selectedMovies.count, 0)
    }
    
    func testAddMovieEvent() throws
    {
        let movie = Movie.fixture()
        
        self.viewModel.send(.initialize)
        XCTAssertEqual(self.viewModel.selectedMovies.count, 0)
        
        self.viewModel.send(.addMovie(movie))
        XCTAssertEqual(self.viewModel.selectedMovies.count, 1)
    }
    
    func testQueryChangedEvent() throws
    {
        self.session.register(stub: MockHTTPSession.Stub(path: "/", method: .get, statusCode: 200, data: try SearchRequest.fixture()))
        
        XCTAssertTrue(self.viewModel.movies.isEmpty)
        self.viewModel.send(.queryChanged("test"))
        self.expectToEventually(!self.viewModel.movies.isEmpty)
        XCTAssertEqual(self.session.numberOfRequests(matching: "/"), 1)
    }
}
