//
//  PlaylistScreenTests.swift
//  MoviePlaylistTests
//
//  Created by Red Davis on 31/01/2021.
//

import Combine
import XCTest
@testable import MoviePlaylist


class PlaylistScreenTests: XCTestCase
{
    // Private
    private var store: DataStore!
    private var viewModel: ViewModel<PlaylistScreen.State, PlaylistScreen.Event, PlaylistScreen.Environment>!
    
    // Setup
    
    override func setUpWithError() throws
    {
        self.store = DataStore()
        self.viewModel = ViewModel(initialState: PlaylistScreen.State(),
                                   reducer: PlaylistScreen.reducer,
                                   environment: PlaylistScreen.Environment(store: self.store))
    }

    // MARK: Tests
    
    func testMoviesChangingEvent() throws
    {
        XCTAssertEqual(self.viewModel.movies.count, 0)
        self.viewModel.send(.moviesChanged([.fixture()]))
        XCTAssertEqual(self.viewModel.movies.count, 1)
    }
    
    func testInitializationEvent() throws
    {
        self.viewModel.send(.initialize)
        
        XCTAssertEqual(self.viewModel.movies.count, 0)
        self.store.add(movie: .fixture())
        self.expectToEventually(self.viewModel.movies.count == 1)
    }
    
    func testRemovingMovieEvent() throws
    {
        self.viewModel.send(.initialize)
        XCTAssertEqual(self.viewModel.movies.count, 0)
        
        let movie = Movie.fixture()
        self.store.add(movie: movie)
        self.expectToEventually(self.viewModel.movies.count == 1)
        
        self.viewModel.send(.removeMovie(movie))
        self.expectToEventually(self.viewModel.movies.isEmpty)
    }
}
