//
//  XCTestCase+Extension.swift
//  MoviePlaylistTests
//
//  Created by Red Davis on 31/01/2021.
//

import Foundation
import XCTest


extension XCTestCase
{
    func expectToEventually(_ test: @autoclosure () -> Bool, timeout: TimeInterval = 1.0, message: String = "")
    {
        let runLoop = RunLoop.current
        let timeoutDate = Date(timeIntervalSinceNow: timeout)
        repeat
        {
            if test()
            {
                return
            }
            
            runLoop.run(until: Date(timeIntervalSinceNow: 0.01))
        } while Date().compare(timeoutDate) == .orderedAscending
        
        XCTFail(message)
    }
    
    func expectToEventuallyReturn<T>(_ test: @autoclosure () -> T?, timeout: TimeInterval = 1.0, message: String = "") throws -> T
    {
        let runLoop = RunLoop.current
        let timeoutDate = Date(timeIntervalSinceNow: timeout)
        repeat
        {
            if let object = test()
            {
                return object
            }
            
            runLoop.run(until: Date(timeIntervalSinceNow: 0.01))
        } while Date().compare(timeoutDate) == .orderedAscending
        
        XCTFail(message)
        throw ExpectToEventuallyError.objectNeverReturned
    }
}


// MARK: Expect eventually error

extension XCTest
{
    enum ExpectToEventuallyError: Error
    {
        case objectNeverReturned
    }
}
