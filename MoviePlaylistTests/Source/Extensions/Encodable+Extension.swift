//
//  Encodable+Extension.swift
//  MoviePlaylistTests
//
//  Created by Red Davis on 31/01/2021.
//

import Foundation


extension Encodable
{
    func encode(with encoder: JSONEncoder = JSONEncoder()) throws -> Data
    {
        try encoder.encode(self)
    }
}
