//
//  SearchRequest+Fixture.swift
//  MoviePlaylistTests
//
//  Created by Red Davis on 31/01/2021.
//

import Foundation
@testable import MoviePlaylist


extension SearchRequest
{
    static func fixture() throws -> Data
    {
        let url = Bundle(for: TestTargetClass.self).url(forResource: "SearchRequest", withExtension: "json")!
        return try Data(contentsOf: url)
    }
}
