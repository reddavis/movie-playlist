//
//  MockResponse.swift
//  Red Davis
//
//  Created by Red Davis on 28/01/2021.
//

import Foundation


struct MockResponse: Codable
{
    // Internal
    var value = "value"
}
