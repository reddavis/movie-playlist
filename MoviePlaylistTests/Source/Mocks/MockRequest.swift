//
//  MockRequest.swift
//  Red Davis
//
//  Created by Red Davis on 28/01/2021.
//

import Foundation
@testable import MoviePlaylist


struct MockRequest: HTTPRequest
{
    // Internal
    typealias ResponseObject = MockResponse
    typealias ErrorObject = MovieDatabaseAPIError
    
    var method: HTTPMethod = .get
    var path = "/test"
    var isAuthenticated = false
    var body: Data?
    var session: HTTPSession = MockHTTPSession()
    var handlerDispatchQueue: DispatchQueue = .main
}
