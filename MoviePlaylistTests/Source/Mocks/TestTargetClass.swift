//
//  TestTargetClass.swift
//  MoviePlaylistTests
//
//  Created by Red Davis on 31/01/2021.
//

import Foundation


/// This is simply a class in the test target
/// so that we can use:
/// `Bundle(for: TestTargetClass.self)` to access resources
/// in this target.
final class TestTargetClass { }
